pragma solidity ^0.4.2;

contract Adoption {

	address[16] public adopters; // an array of addresses with a length of 16

	// function for adopting a pet
	function adopt(uint petId) public returns (uint) {
		require(petId >= 0 && petId <= 15); // since array is = 16
		adopters[petId] = msg.sender; // msg.sender = address of person or smart contract who called function
		return petId;

	}

	// retrieving adopters
	function getAdopters() public view returns (address[16]) {
		return adopters;
	}

}